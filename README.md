# CS371p: Object-Oriented Programming Grades Repo

* Name: Ahmad Hadidi

* EID: afh783

* GitLab ID: afhadidi22

* HackerRank ID: afhadidi

* Git SHA: ef65c5a2a0eb00216dfeb5f6522c900251ab15be

* GitLab Pipelines: https://gitlab.com/afhadidi22/cs371p-grades/-/jobs/6067379441

* Estimated completion time: 12 hours

* Actual completion time: 15 hours

* Comments: N/A
