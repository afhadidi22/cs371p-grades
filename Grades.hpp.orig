// ----------
// Grades.hpp
// ----------

#ifndef Grades_hpp
#define Grades_hpp

// --------
// includes
// --------

#include <algorithm> // count, min_element, transform
#include <cassert>   // assert
#include <map>       // map
#include <string>    // string
#include <vector>    // vector

using namespace std;

//Constants for the EMRN scale
const int E = 3;
const int M = 2;
const int R = 1;
const int N = 0;

const int NUM_DIF_GRADES = 11;
const int NUM_SECTIONS = 5;
const int TEST_SECTION = 0;
const int EXERCISE_SECTION = 1;
const int BLOG_SECTION = 2;
const int PAPER_SECTION = 3;
const int QUIZ_SECTION = 4;

string determine_grade(int (&scores_of_sections)[NUM_SECTIONS]);
void extract_scores(int (&scores_of_sections)[NUM_SECTIONS], const vector<vector<int>>& v_v_scores);
int emr_scores(const vector<int>& given_scores);


// -----------
// grades_eval
// -----------

string grades_eval (const vector<vector<int>>& v_v_scores) {
    assert(&v_v_scores); //assert that &v_v_scores is not null and therefore exists //ASK ABOUT THIS
    int scores_of_sections[NUM_SECTIONS] {};
    extract_scores(scores_of_sections, v_v_scores);
    string grade = determine_grade(scores_of_sections);
    return grade;
}

/**
 * This method takes in the total number of es and ms per section and returns the 
 * grade this set of grades recieves
 * 
 *  @param scores_of_sections a integer array of size 5 with the number of es and ms recieved in each section
 *  @return grades[worst_so_far] the grade recieved by this particular set of grades
 * 
*/

string determine_grade(int (&scores_of_sections)[NUM_SECTIONS]){
    //ensure scores_of_sections is not null
    assert(scores_of_sections);
    //declare arrays for requirements and grades
    int requirements[][NUM_DIF_GRADES] = { 
        {5, 5, 4, 4, 4, 4, 4, 4, 3, 3, 3},
        {11, 11, 10, 10, 10, 9, 9, 8, 8, 8, 7},
        {13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8},
        {13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8},
        {39, 38, 37, 35, 34, 32, 31, 29, 28, 27, 25}
    };
    string grades[] = {"A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-", "F"};

    //go through all the sections and find the worst one to base grade off of
    int worst_so_far = -1;
    for(int i = 0; i < NUM_SECTIONS; i++){
        int curr = scores_of_sections[i];
        int j = 0;
        while(j < NUM_DIF_GRADES && curr < requirements[i][j]){
            j++;
        }
        worst_so_far = max(worst_so_far, j);

        //If F return immediately
        if(worst_so_far == NUM_DIF_GRADES + 1){
            return grades[worst_so_far];
        }
    }
    return grades[worst_so_far];
}

/**
 * Method that takes in each grade recieved by a student in class and runs emr scores to get the number of es and ms
 * in each category 
 * 
 * @param scores_of_sections a array of size 5 that will be populated with the number of es and ms in each section
 *         v_v_scores a vector of vector of ints that contains every grade recieved in class
*/

void extract_scores(int (&scores_of_sections)[NUM_SECTIONS], const vector<vector<int>>& v_v_scores){
    
    assert(scores_of_sections);
    assert(&v_v_scores);

    for(int i = 0; i < NUM_SECTIONS; i++){
        scores_of_sections[i] = emr_scores(v_v_scores[i]);
    }
}


/**
 * This function returns the count used to determine grades for grades that use emr scoring
 * meaning es can make up rs
 * 
 * @param given_scores the scores recieved for a single category of grading(exercises blogs and papers)
 * @return integer of the number of es and ms acheived by the students
*/
int emr_scores(const vector<int>& given_scores){
    
    assert(&given_scores);
    //Extract scores from the vector and turn it into an array of the counts of each grade
    int scores[4] = {0};
    int size = given_scores.size();
    for(int i = 0; i < size; i++) {
        scores[given_scores[i]]++;
    }
    
    //Adjust the Es to make up the Rs
    int num_makeups = scores[E] / 2;
    while(num_makeups > 0){
        if(scores[R] > 0){
            scores[R]--;
            scores[M]++;
        }
        num_makeups--;
    }

    assert(num_makeups == 0);

    //Final score from this section of grading
    return scores[E] + scores[M];
}

#endif // Grades_hpp
