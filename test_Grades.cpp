// ---------------
// test_Grades.cpp
// ---------------

// --------
// includes
// --------

#include <map>    // map
#include <string> // string
#include <vector> // vector

#include "gtest/gtest.h"

#include "Grades.hpp"

using namespace std;

// ----------------
// test_grades_eval
// ----------------

TEST(grades_eval, test_0) {
    const vector<vector<int>> v_v_scores = {{2, 2, 2, 0, 0}, {3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3}, {3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3}, {3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3},
        {3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3}
    };
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "D+");
}

TEST(emr_scores, test_1) {
    const vector<int> v_v_scores = {3, 3, 3, 2, 2, 2, 1};
    const int  num_es_and_ms     = emr_scores(v_v_scores);
    ASSERT_EQ(num_es_and_ms, 7);
}

TEST(emr_scores, test_2) {
    const vector<int> v_v_scores = {3, 3, 3, 2, 2, 2, 1, 1};
    const int  num_es_and_ms     = emr_scores(v_v_scores);
    ASSERT_EQ(num_es_and_ms, 7);
}

TEST(emr_scores, test_3) {
    const vector<int> v_v_scores = {3, 3, 1, 1, 1, 1, 1};
    const int  num_es_and_ms     = emr_scores(v_v_scores);
    ASSERT_EQ(num_es_and_ms, 3);
}

TEST(emr_scores, test_4) {
    const vector<int> v_v_scores = {3, 2, 2, 2, 2, 2, 1};
    const int  num_es_and_ms     = emr_scores(v_v_scores);
    ASSERT_EQ(num_es_and_ms, 6);
}

TEST(emr_scores, test_5) {
    const vector<int> v_v_scores = {3, 2, 2, 2, 2};
    const int  num_es_and_ms     = emr_scores(v_v_scores);
    ASSERT_EQ(num_es_and_ms, 5);
}

TEST(determine_grade, test_6) {
    int scores[] = {5, 12, 14, 14, 42};
    const string              letter     = determine_grade(scores);
    ASSERT_EQ(letter, "A");
}

TEST(determine_grade, test_7) {
    int scores[] = {5, 6, 14, 14, 42};
    const string              letter     = determine_grade(scores);
    ASSERT_EQ(letter, "F");
}

TEST(determine_grade, test_8) {
    int scores[] = {5, 9, 14, 10, 42};
    const string              letter     = determine_grade(scores);
    ASSERT_EQ(letter, "C");
}

TEST(determine_grade, test_9) {
    int scores[] = {3, 12, 14, 14, 42};
    const string              letter     = determine_grade(scores);
    ASSERT_EQ(letter, "D+");
}

TEST(determine_grade, test_10) {
    int scores[] = {5, 12, 14, 14, 37};
    const string              letter     = determine_grade(scores);
    ASSERT_EQ(letter, "B+");
}

TEST(determine_grade, test_11) {
    int scores[] = {4, 10, 11, 11, 42};
    const string              letter     = determine_grade(scores);
    ASSERT_EQ(letter, "B-");
}

